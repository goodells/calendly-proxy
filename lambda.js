const serverlessExpress = require('@vendia/serverless-express');
const { application } = require('./build/application');

exports.handler = serverlessExpress({ app: application });

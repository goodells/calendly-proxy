export const CALENDLY_API_ROOT = 'https://api.calendly.com';
export const { CALENDLY_TOKEN } = process.env;

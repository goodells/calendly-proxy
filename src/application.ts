import axios, { isAxiosError } from 'axios';
import * as moment from 'moment';
import * as express from 'express';
import * as cors from 'cors';
import * as _ from 'lodash';
import { CALENDLY_API_ROOT, CALENDLY_TOKEN } from './constants';

const axiosInstance = axios.create({
  baseURL: CALENDLY_API_ROOT,
  headers: {
    Authorization: `Bearer ${CALENDLY_TOKEN}`,
  },
});

const getCurrentUser = async () => {
  const response = await axiosInstance.get('/users/me');
  const { data } = response;

  return data;
};

const getEventTypes = async ({ userUri }: { userUri: string }) => {
  const response = await axiosInstance.get('/event_types', {
    params: {
      user: userUri,
    },
  });
  const { data } = response;
  const eventTypes = data.collection
    .filter((eventType) => eventType.secret !== true)
    .map((eventType) => _.omit(eventType, 'internal_note'));

  return {
    ...data,
    collection: eventTypes,
  };
};

const getEventTypeAvailableTimes = async ({
  eventTypeUri,
  startTime,
  endTime,
}: {
  eventTypeUri: string;
  startTime: moment.Moment;
  endTime: moment.Moment;
}) => {
  const response = await axiosInstance.get('/event_type_available_times', {
    params: {
      event_type: eventTypeUri,
      start_time: startTime.toISOString(),
      end_time: endTime.toISOString(),
    },
  });
  const { data } = response;

  return data;
};

const application = express();
application.use(cors());

application.get('/event_types', async (request, response, next) => {
  try {
    const user = await getCurrentUser();
    const eventTypesData = await getEventTypes({ userUri: user.resource.uri });

    response.json(eventTypesData);
  } catch (error) {
    next(error);
  }
});
application.get<
  {},
  {},
  {},
  { event_type: string; start_time: string; end_time: string }
>('/event_type_available_times', async (request, response, next) => {
  try {
    const {
      event_type: eventTypeUri,
      start_time: startTimeString,
      end_time: endTimeString,
    } = request.query;
    const availableTimesData = await getEventTypeAvailableTimes({
      eventTypeUri,
      startTime: moment.utc(startTimeString),
      endTime: moment.utc(endTimeString),
    });

    response.json(availableTimesData);
  } catch (error) {
    next(error);
  }
});

application.use((error, request, response, next) => {
  if (isAxiosError(error)) {
    response.status(error.response.status).json(error.response.data);
  } else {
    response.status(500).json({
      error: {
        message: error?.message ?? 'Unknown error',
      },
    });
  }
});

export { application };
